import './App.scss';
import MainPage from "./components/mainpage/MainPage";
import {useEffect, useState} from "react";
import {useDispatch} from "react-redux";
import actions from "./components/actions";
import {Routes, Route, BrowserRouter, Navigate} from "react-router-dom";
import Payment from "./components/mainpage/routerComponents/Payment";
import Cart from "./components/mainpage/routerComponents/Cart";
import Review from "./components/mainpage/routerComponents/Review";
import SingleProduct from "./components/mainpage/routerComponents/singe-page-product/SingleProduct";
import Header from "./components/header/Header";
import React from "react";
import PageNotFound from "./components/mainpage/routerComponents/pageNotFound";

function App() {

    const [isLoading, setIsLoading] = useState(true);
    const dispatch = useDispatch();


    // Fetch data by Redux thunk
    // [fetchOneProduct] is the function that will be executed by Redux thunk middleware
    // this is purpose of the redux thunk which lets you dispatch a function as an action

    //fetch all products
    useEffect( () => {
        dispatch(actions.productActions.fetchAllProducts())
            //works as a cleanup function
            .then(()=> setIsLoading( false))
    },[]);

    // Fetch all filters
    useEffect(() => {
        dispatch(actions.filterActions.getFilters())
            .then(()=> setIsLoading(false))
    },[]);

  return isLoading ? (
    <div>loading...</div>
  ) : (
      <div className="App">
          <Header />
          <Routes>
              <Route path="/" element={<Navigate to="mainPage/1" />}/>
              <Route path='/mainPage/:pageNum' element={<MainPage />}/>
              <Route path='/singleProduct' element={<SingleProduct />}/>
              <Route path='/cart' element={<Cart />}/>
              <Route path='/review' element={<Review />}/>
              <Route path='/payment' element={<Payment />}/>

              <Route path="*" element={<PageNotFound />}></Route>
          </Routes>
      </div>
  );
}

export default App;
