import React from 'react';

const Notification = () => {
    return (
        <>
            <div className="notification">
                <p>Need it fast? Use <strong>Available near you </strong> to buy and pick up in store</p>
            </div>
        </>
    );
};

export default Notification;