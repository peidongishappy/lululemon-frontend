import React from 'react';
import {useSelector} from "react-redux";
import {productReducer} from "../reducers/productReducer";

const SortingTabs = () => {
    const products = useSelector(state => state?.productReducer?.allProducts)

    return (
        <>
            <div className="sortingTabs">
                {
                    !!products &&
                <div className="options">
                    {`All Items (${!!products?.pageParams ? products?.pageParams?.totalProducts : products.products?.length})`}
                </div>
                }
                <div className="options">Available Near You</div>
                <hr/>
            </div>
        </>
    );
};

export default SortingTabs;