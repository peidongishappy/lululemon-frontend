import React from 'react';
import './style/mainpage.scss';
import LeftContainer from "./routerComponents/filter-container/LeftContainer";
import RightContainer from "./routerComponents/product-display-container/RightContainer";
import {useNavigate} from "react-router-dom"


const MainPage = () => {
    const navigate = useNavigate();
    // console.log('print product info', productInfo[0]);
    return (
        <>
            <div className="button">
                    <button onClick={()=> {
                        navigate('/singleProduct')
                    }}>Single Product page</button>
            </div>
            <div className="mainContainer">
                <LeftContainer/>
                <RightContainer />
            </div>
        </>
    );
};

export default MainPage;