import React from 'react';
import '../../style/productTiles.scss';
import {useSelector} from "react-redux";
import ProductTile from "./ProductTile";

const ProductTiles = () => {
    // const oneProduct = useSelector(state => state?.productReducer?.one_product)
    const allProducts = useSelector(state => state?.productReducer?.allProducts)
    console.log('useSelector products from state', allProducts)

    // grab first 100 products
    const firstHundred = allProducts?.products?.slice(0, 100) || [];
    // console.log('[firstHundred]', allProducts.products.slice(0, 100))

    return (
        <>
            <div className="productTiles">
                {
                    firstHundred.map((product, index) => (
                        <ProductTile oneProduct={product}
                                     key={index}
                        />)
                    )
                }
            </div>
        </>
    );
};

export default ProductTiles;