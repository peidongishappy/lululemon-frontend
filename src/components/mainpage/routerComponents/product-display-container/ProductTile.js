import '../../style/productTile.css';
import React, {useEffect, useState} from 'react';
import MainCarousel from "./MainCarousel";

/*
 * ProductTile component
*/

const ProductTile = ({oneProduct}) => {
    // Test if the component receives the data
    // console.log('[productTile] products', oneProduct);
    const imgArray = oneProduct?.images;
    // console.log(`imgArray`, imgArray)
    const swatchesArray = oneProduct?.swatches;
    const productName = oneProduct?.name;
    const productPrice = oneProduct?.price;
    // console.log('[productTile] images',imgArray)
    // console.log('[ProductTile] all swatches', swatchesArray);

    // needs to initialize both img and selectedSwatch in order to render children components
    const [currentIndex, setCurrentIndex] = useState(0)
    // const [img, setImg] = useState(imgArray[0]);
    // const [selectedSwatch, setSelectedSwatch] = useState(swatchesArray[0]);
    // console.log('[ProductTile] selected initial swatch', selectedSwatch)


    const setAllSwatches = () => {
        //Very easy to make mistake here in onMouseEnter, it is swatch not swatch.colorId in setSelectedSwatch
       return swatchesArray.map((swatch,index) =>(
            <img src={swatch.swatch}
                 alt={swatch.swatchAlt}
                 key={swatch.colorId}
                 // onMouseEnter={()=> setSelectedSwatch(swatch)}
                onMouseEnter={()=>setCurrentIndex(index)}
            />
            )
        )
    }
    // console.log('[productTile] selected swatch', selectedSwatch)
    // console.log('[productTile] img', img)
    return (
        <>
                <div className="productTile">
                        {/* why we use index:
                             - useState: race condition
                             - index: imgArray will be updated when redux updates
                         */}
                        <MainCarousel img={imgArray[currentIndex]}/>
                    {/*Show all the swatches*/}
                    <div className="swatches">
                        <div className="swatchCircle">
                            {setAllSwatches()}
                        </div>
                    </div>
                    <div className="name">
                        {productName}
                    </div>
                    <div className="price">
                        {productPrice}
                    </div>
                </div>

        </>
    );
};

export default ProductTile;