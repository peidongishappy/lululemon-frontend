import React from 'react';
import ProductTiles from "./ProductTiles";
import Notification from "../../Notification";
import SortingTabs from "../../SortingTabs";
import fpIMG from "../../../helpers/img/fpIMG.png";

const RightContainer = () => {
    return (
        <>
            <div className="rightContainer">
                <div className="first-page-img">
                    <img src={fpIMG} alt="display-img"/>
                </div>
                <Notification/>

                {/*main product content*/}
                <div className="productContent">
                    <SortingTabs/>
                    <ProductTiles />
                </div>
            </div>
        </>
    );
};

export default RightContainer;