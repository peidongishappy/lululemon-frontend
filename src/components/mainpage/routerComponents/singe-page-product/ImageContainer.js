import React from 'react';

const ImageContainer = () => {
    return (
        <>
            <div className="image-container">
                <div className="product-img">
                    Carousel Image
                </div>
            </div>
        </>
    );
};

export default ImageContainer;