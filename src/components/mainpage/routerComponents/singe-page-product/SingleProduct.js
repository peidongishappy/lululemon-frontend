import React from 'react';
import ImageContainer from "./ImageContainer";
import SelectionContainer from "./SelectionContainer";

const SingleProduct = () => {
    return (
        <>
            <ImageContainer/>
            <SelectionContainer/>
        </>
    );
};

export default SingleProduct;