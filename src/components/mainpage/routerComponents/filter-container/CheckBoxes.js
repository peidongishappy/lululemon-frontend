import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {observe} from "web-vitals/dist/modules/lib/observe";
import actions from "../../../actions";

const CheckBoxes = ({val}) => {
    const dispatch = useDispatch()
    // console.log('[val]', val);
    const allFilters = useSelector(state => state?.productReducer?.filters)
    // allFilters = {}

    const entries = Object.entries(val);
    //key: index, value: {name:..., isChecked:...}
    // console.log('entries', entries)

    const handlerCheckBoxChange = (entry) => {
        // console.log('entry in handler', entry)
       actions.filterActions.updateFilter(dispatch)(entry)
           .then( rs => {
           rs && actions.productActions.fetchAllProductsWithFilter(dispatch)(allFilters)
       })
    }

        return (
            <>
                <div className="checkBox">
                    {entries.map(([key, value], index) => {
                            return <div key={index}>
                                <label htmlFor={`${value.name}${index}`}>
                                    <input type="checkbox"
                                           id={`${value.name}${index}`}
                                           name={value.name}
                                           onChange={() => {
                                               handlerCheckBoxChange(value.name)
                                           }}
                                    />
                                    {value.name}
                                </label>
                            </div>

                        }
                    )}
                </div>
            </>
        );
    };

export default CheckBoxes