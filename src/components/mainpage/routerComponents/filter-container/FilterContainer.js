import React, {useEffect, useState} from 'react';
import CheckBoxes from "./CheckBoxes";

const FilterContainer = ({filters}) => {
    // console.log('[FilterContainer]', filters);
    const [status, setStatus] = useState(false)
    const filterObjects = Object.keys(filters).length !== 0 ? filters : {};
    // entries are key/val pairs
    const entries = Object.entries(filterObjects);
    console.log('[Entries]', entries)



    // console.log('[FilterObjects]', filterObjects);
    // filterObjects is an object {Gender:Array(2), Category:Array(33)...}
    // console.log('[test Object.values(filterObjects)', Object.values(filterObjects))

    useEffect(() => {
        console.log(filters)
        setStatus(Object.keys(filters).length !== 0);
    }, [filters])


    // Major issues: Objects are not valid as a React child when rendering checkboxes
    // How did I fix it:
    // 1. use Object.entries and get key/value pairs then use Map function
    // 2. create a separate component for better understanding and reduce errors during coding

    return status ? (
        <>
            {/*
                - filterObjects: is an object {Gender:Array(2), Category:Array(33)...}
                - since filterObjects is an object, I cannot use map function
                - props: obj, single object Gender:[Array(2)]
                */}
            <div className="filter-type">
                {
                    entries.map(([key, val], idx) => {
                        return <div className='typeContainer' key={idx}>
                            <div className='type-name'>
                                {key}
                            </div>
                            <CheckBoxes val={val}/>
                        </div>
                    }
                    )
                }
            </div>

            <div className='titleBreakLine'><hr/></div>
        </>) : (<div>Loading...</div>)
}
    export default FilterContainer;
