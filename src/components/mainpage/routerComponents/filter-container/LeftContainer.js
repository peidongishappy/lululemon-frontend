import React, {useEffect, useState} from 'react';
import {useSelector} from "react-redux";
import FilterContainer from "./FilterContainer";

const LeftContainer = () => {

    // Fetch all filters from the global state
    // I kept the entire data from API because I will use status
    const allFilters = useSelector(state => state?.productReducer?.filters)
    // console.log('[LeftContainer] Global store: allFilters', allFilters);

    // Object.entries method is able to access the key/value pairs
    // Object.keys method is able to access the key/value pairs

    return (
        <>

            <div className="leftContainer">
                <div className="categoryPageTitle">
                    <h2>What's New</h2>
                    <div className='titleBreakLine'><hr/></div>
                </div>
                {/* Dynamically create filters */}
                <FilterContainer filters={allFilters}/>
            </div>
        </>
    );
};

export default LeftContainer;