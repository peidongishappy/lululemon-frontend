import React from 'react';
import './style/header.scss';
import LocalMallOutlinedIcon from '@mui/icons-material/LocalMallOutlined';
import PersonAddAltOutlinedIcon from '@mui/icons-material/PersonAddAltOutlined';
import {Link} from "react-router-dom";

const Header = () => {
    return (
        <>
            <div className="topNavigation">
                <ul id="headerList">
                    <li id='1'>Store Locator</li>
                    <li id='2'>
                        <PersonAddAltOutlinedIcon />
                        Sign In
                    </li>
                    <li id='3'>Wish List</li>
                    <li id='4'>Gift Cards</li>
                    <li id='5'>CAN</li>
                </ul>
            </div>
            <div className="navDesktop">
                <div className="navLogo">
                    <Link to={'/'}>
                        <img id='logo'
                             src="https://toppng.com/uploads/preview/lululemon-logo-angel-tube-statio-11562943902y378g3sht3.png"
                             alt="logo"/>
                    </Link>
                </div>
                <div className="primaryNavigation">
                    <ul>
                        <li>WOMEN</li>
                        <li>MEN</li>
                        <li>ACCESSORIES</li>
                        <li>SHOES</li>
                        <li>STUDIO</li>
                    </ul>
                </div>
                <div className="secondaryNavigation">
                    <form autoComplete='off' action='' className='searchForm'>
                        <input className='searchInput' type="text" placeholder='search'/>
                    </form>
                </div>
                <div className="cart">
                    <LocalMallOutlinedIcon />
                </div>
            </div>

        </>
    );
};

export default Header;