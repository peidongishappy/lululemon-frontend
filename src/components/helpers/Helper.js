export const FETCH_ONE_URL = 'http://api-lulu.hibitbyte.com/product/prod10550089?mykey=fuM1J1/wOUzyoUbxEYRZ2%2BdRoHepR7z%2BGDfhqB2%2BECqoI99i2RVYZAUcdVpTod96Wyf4wqsh8dR1GTFvHAdJTw==';
export const FETCH_ALL_URL = 'http://api-lulu.hibitbyte.com/product/allProducts?mykey=fuM1J1/wOUzyoUbxEYRZ2%2BdRoHepR7z%2BGDfhqB2%2BECqoI99i2RVYZAUcdVpTod96Wyf4wqsh8dR1GTFvHAdJTw=='
export const GET_FILTERS_URL = 'http://api-lulu.hibitbyte.com/product/filter?mykey=fuM1J1/wOUzyoUbxEYRZ2%2BdRoHepR7z%2BGDfhqB2%2BECqoI99i2RVYZAUcdVpTod96Wyf4wqsh8dR1GTFvHAdJTw=='
export const actionType = {
    //Product action defined
    'FETCH_ONE_PRODUCT' : 'FETCH_ONE_PRODUCT',
    'FETCH_ALL_PRODUCTS' : 'FETCH_ALL_PRODUCT',
    'UPDATE_FILTERS' : 'UPDATE_FILTERS',
    'GET_FILTERS' : 'GET_FILTERS',
    'CHECK_BOX_UPDATE' : 'CHECK_BOX_UPDATE',
    'FETCH_ALL_PRODUCTS_WITH_FILTER':'FETCH_ALL_PRODUCTS_WITH_FILTER'
}

