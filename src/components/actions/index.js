import productActions from './productActions'
import filterActions from "./filterActions";

export default {
    productActions,
    filterActions
}